﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketsLib;

namespace WebSocketsCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            WebSocketsMain.Initialize(6378);

            while (true)
            {
                switch (Console.ReadKey().KeyChar)
                {
                    case 'a':
                        Console.Write("Send to all - Message: ");
                        WebSocketsMain.SendToAll(Console.ReadLine());
                        continue;
                    case 's':
                        Console.Write("Send to name: ");
                        var name = Console.ReadLine();
                        Console.Write("Message: ");
                        WebSocketsMain.SendToUser(name, Console.ReadLine());
                        continue;
                    case 'q':
                        break;
                }

                Console.WriteLine("Press enter to terminate");
                Console.ReadLine();
                WebSocketsMain.Shutdown();
                break;
            }


        }
    }
}

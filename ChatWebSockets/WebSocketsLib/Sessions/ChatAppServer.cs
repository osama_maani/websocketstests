﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperSocket.WebSocket;

namespace WebSocketsLib.Sessions
{
    public class ChatAppServer : WebSocketServer<ChatSession>
    {
        public ChatAppServer()
        {
            Console.WriteLine("Chat App Server Initiated");
        }
    }
}

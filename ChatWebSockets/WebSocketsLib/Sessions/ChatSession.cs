﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperSocket.WebSocket;

namespace WebSocketsLib.Sessions
{
    public class ChatSession : WebSocketSession<ChatSession>
    {
        public string Name;

        protected override void OnSessionStarted()
        {
            Name = Path.TrimStart('/');
            Console.WriteLine("New session connected...");
            //base.OnSessionStarted();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperSocket.WebSocket;
using WebSocketsLib.Sessions;

namespace WebSocketsLib
{
    public class WebSocketsMain
    {
        private static readonly ChatAppServer Server = new ChatAppServer();
        public static void Initialize(int port)
        {
            if (!Server.Setup(port))
            {
                Console.WriteLine($"Failed to setup WebSocketServer on port {port}");
                return;
            }

            Console.WriteLine($"WebSocketServer initialized on port {port}");
            Server.NewMessageReceived += Server_NewMessageReceived;

            if (!Server.Start())
            {
                Console.WriteLine($"Failed to start WebSocketServer");
                return;
            }
            Console.WriteLine("WebSocketServer Started");
        }

        public static bool SendToAll(string message)
        {
            return Server.GetAllSessions().Aggregate(false, (b, session) => 
                session.Connected ? session.TrySend(message) || b : b);
        }

        public static bool SendToUser(string name, string message)
        {
            return Server.GetSessions(s => s.Connected && s.Name == name)
                .Aggregate(true, (b, session) => b && session.TrySend(message));
        }

        public static void Shutdown()
        {
            Server.Stop();
        }

        private static void Server_NewMessageReceived(ChatSession session, string value)
        {
            Console.WriteLine($"- {session.Name} Said: {value}");
            session.Send("Server: OK");
        }
    }
}
